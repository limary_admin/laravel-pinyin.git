<?php
namespace Sinta\Pinyin;

use Closure;
use Generator;
use SplFileObject;

/**
 * 通用文件字典加载器
 *
 * Class GeneratorFileDictLoader
 * @package Sinta\Pinyin
 */
class GeneratorFileDictLoader implements DictLoaderInterface
{

    protected $path;

    protected $segmentName = 'words_%s';


    protected static $handles = [];

    protected static $surnamesHandle;


    public function __construct($path)
    {
        $this->path = $path;

        for ($i = 0; $i < 100; ++$i) {
            $segment = $this->path.'/'.sprintf($this->segmentName, $i);

            if (file_exists($segment) && is_file($segment)) {
                array_push(static::$handles, $this->openFile($segment));
            }
        }
    }

    protected function openFile($filename, $mode = 'r')
    {
        return new SplFileObject($filename, $mode);
    }


    protected function getGenerator(array $handles)
    {
        foreach ($handles as $handle) {
            $handle->seek(0);
            while (false === $handle->eof()) {
                $string = str_replace(['\'', ' ', PHP_EOL, ','], '', $handle->fgets());

                if (false === strpos($string, '=>')) {
                    continue;
                }

                list($string, $pinyin) = explode('=>', $string);

                yield $string => $pinyin;
            }
        }
    }

    protected function traversing(Generator $generator, Closure $callback)
    {
        foreach ($generator as $string => $pinyin) {
            $callback([$string => $pinyin]);
        }
    }


    public function map(Closure $callback)
    {
        $this->traversing($this->getGenerator(static::$handles), $callback);
    }


    public function mapSurname(Closure $callback)
    {
        if (!static::$surnamesHandle instanceof SplFileObject) {
            static::$surnamesHandle = $this->openFile($this->path.'/surnames');
        }

        $this->traversing($this->getGenerator([static::$surnamesHandle]), $callback);
    }



}