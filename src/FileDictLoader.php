<?php
namespace Sinta\Pinyin;

use Closure;

/**
 *
 *
 * Class FileDictLoader
 * @package Sinta\Pinyin
 */
class FileDictLoader implements DictLoaderInterface
{

    protected $segmentName = 'words_%s';


    protected $path;


    public function __construct($path)
    {
        $this->path = $path;
    }


    public function map(Closure $callback)
    {
        for ($i = 0; $i < 100; ++$i) {
            $segment = $this->path.'/'.sprintf($this->segmentName, $i);
            if (file_exists($segment)) {
                $dictionary = (array) include $segment;
                $callback($dictionary);
            }
        }
    }

    public function mapSurname(Closure $callback)
    {
        $surnames = $this->path.'/surnames';
        if (file_exists($surnames)) {
            $dictionary = (array) include $surnames;
            $callback($dictionary);
        }
    }

}