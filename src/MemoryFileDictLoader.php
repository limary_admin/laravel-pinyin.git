<?php
namespace Sinta\Pinyin;

use Closure;

/**
 * 内存文件字典加载器
 *
 * Class MemoryFileDictLoader
 * @package Sinta\Pinyin
 */
class MemoryFileDictLoader implements DictLoaderInterface
{
    protected $path;

    protected $segmentName = 'words_%s';

    protected $segments = array();

    protected $surnames = array();


    public function __construct($path)
    {
        $this->path = $path;

        for ($i = 0; $i < 100; ++$i) {
            $segment = $path.'/'.sprintf($this->segmentName, $i);

            if (file_exists($segment)) {
                $this->segments[] = (array) include $segment;
            }
        }
    }


    public function map(Closure $callback)
    {
        foreach ($this->segments as $dictionary) {
            $callback($dictionary);
        }
    }


    public function mapSurname(Closure $callback)
    {
        if (empty($this->surnames)) {
            $surnames = $this->path.'/surnames';

            if (file_exists($surnames)) {
                $this->surnames = (array) include $surnames;
            }
        }

        $callback($this->surnames);
    }
}